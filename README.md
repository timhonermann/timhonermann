<div style="display: flex; flex-direction: column; align-items: center">
    <h1>Hi 👋, I'm Tim</h1>
    <h3>A passionate fullstack developer from Lucerne, Switzerland <img align="center" src="https://cdn-icons-png.flaticon.com/512/555/555582.png" alt="Switzerland" width="30"/> </h3>
</div>
<br/>

- 🔭 I’m currently working on [Bookish List](https://gitlab.com/bookish-list)

- 🌱 I’m currently learning **Spring Security** and **ArchUnit**

- 💬 Ask me about **Angular**

- 📫 How to reach me **tim.honermann@windowslive.com**

- ⚡ Fun fact **I own a military working dog**

<h2 align="left">Connect with me: </h2>
<p align="left">
<a href="https://instagram.com/timhonermann" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/instagram.svg" alt="timhonermann" height="30" width="40" /></a>
</p>

<h2 align="left">Languages and Tools:</h2>
<p align="left">
    <strong>Languages: </strong>
    <a href="https://www.java.com" target="_blank" rel="noreferrer">
        <img align="center" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/java/java-original.svg" alt="java" width="40" height="40" />
    </a>
    <a  href="https://www.typescriptlang.org/" target="_blank" rel="noreferrer">
        <img align="center" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/typescript/typescript-original.svg" alt="typescript" width="40" height="40" />
    </a>
    <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" rel="noreferrer">
        <img align="center" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" alt="javascript" width="40" height="40" />
    </a>
    <a href="https://www.w3.org/html/" target="_blank" rel="noreferrer">
        <img align="center" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="40" height="40" />
    </a>
    <a href="https://www.w3schools.com/css/" target="_blank" rel="noreferrer">
        <img align="center" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original-wordmark.svg" alt="css3" width="40" height="40" />
    </a>
</p>
<p align="left">
    <strong >Frameworks: </strong>
    <a href="https://angular.io" target="_blank" rel="noreferrer">
        <img align="center" src="https://angular.io/assets/images/logos/angular/angular.svg" alt="angular" width="40" height="40" />
    </a>
    <a href="https://spring.io/" target="_blank" rel="noreferrer">
        <img align="center" src="https://www.vectorlogo.zone/logos/springio/springio-icon.svg" alt="spring" width="40" height="40" />
    </a>
    <a href="https://nodejs.org" target="_blank" rel="noreferrer">
        <img align="center" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/nodejs/nodejs-original-wordmark.svg" alt="nodejs" width="40" height="40" />
    </a>
    <a href="https://www.tensorflow.org" target="_blank" rel="noreferrer">
        <img align="center" src="https://www.vectorlogo.zone/logos/tensorflow/tensorflow-icon.svg" alt="tensorflow" width="40" height="40" />
    </a>
</p>
<p align="left">
    <strong >Database: </strong>
    <a href="https://www.postgresql.org" target="_blank" rel="noreferrer">
        <img align="center" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/postgresql/postgresql-original-wordmark.svg" alt="postgresql" width="40" height="40" />
    </a>
    <a href="https://www.mysql.com/" target="_blank" rel="noreferrer">
        <img align="center" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mysql/mysql-original-wordmark.svg" alt="mysql" width="40" height="40" />
    </a>
</p>
<p align="left">
    <strong>Testing: </strong>
    <a href="https://junit.org/junit5/" target="_blank" rel="noreferrer">
        <img align="center" src="https://marcphilipp.de/img/junit5-logo.png" alt="JUnit" width="40" />
    </a>
    <a href="https://jestjs.io" target="_blank" rel="noreferrer">
        <img align="center" src="https://www.vectorlogo.zone/logos/jestjsio/jestjsio-icon.svg" alt="jest" width="40" height="40" />
    </a>
</p>
<p align="left">
    <strong >Other: </strong>
    <a href="https://git-scm.com/" target="_blank" rel="noreferrer">
        <img align="center" src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="40" height="40" />
    </a>
    <a href="https://gohugo.io/" target="_blank" rel="noreferrer">
        <img align="center" src="https://api.iconify.design/logos-hugo.svg" alt="hugo" width="40" height="40" />
    </a>
    <a href="https://firebase.google.com/" target="_blank" rel="noreferrer">
        <img align="center" src="https://www.vectorlogo.zone/logos/firebase/firebase-icon.svg" alt="firebase" width="40" height="40" />
    </a>
</p>
